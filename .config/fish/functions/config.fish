# Defined in - @ line 1
function config --wraps='/usr/bin/git --git-dir=/home/decisivedove/dotfiles/ --work-tree=/home/decisivedove' --description 'alias config /usr/bin/git --git-dir=/home/decisivedove/dotfiles/ --work-tree=/home/decisivedove'
  /usr/bin/git --git-dir=/home/decisivedove/dotfiles/ --work-tree=/home/decisivedove $argv;
end
