#!/bin/sh
#starting utility applications at boot time
xrandr --output DVI-D-0 --set TearFree on
feh --bg-fill $(cat /home/decisivedove/.cache/wal/wal)
corectrl &
nm-applet &
pamac-tray &
xfce4-power-manager &
numlockx on &
# blueberry-tray &
picom --experimental-backends --config $HOME/.config/picom.conf &
/usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1 &
/usr/lib/xfce4/notifyd/xfce4-notifyd &
pasystray &
blueman-tray &
redshift-gtk &
rsibreak &
#kdeconnect-indicator &
