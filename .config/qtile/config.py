# Copyright (c) 2010 Aldo Cortesi
# Copyright (c) 2010, 2014 dequis
# Copyright (c) 2012 Randall Ma
# Copyright (c) 2012-2014 Tycho Andersen
# Copyright (c) 2012 Craig Barnes
# Copyright (c) 2013 horsik
# Copyright (c) 2013 Tao Sauvage
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

from libqtile.config import Key, Screen, Group, Drag, Click, Match
from libqtile.lazy import lazy
from libqtile import layout, bar, widget
from libqtile import hook 
from typing import List  # noqa: F401
import subprocess
import os

mod = "mod4"
myTerm = "alacritty"
@hook.subscribe.startup_once
def autostart():
    startup = os.path.expanduser('~/.config/qtile/autostart.sh')
    subprocess.call([startup])

keys = [
    Key([mod], "Return",
             lazy.spawn(myTerm),
             desc='Launches My Terminal'
             ),
    Key([mod],"z",
             lazy.spawn("dmenu_wal"),
             desc='Dmenu Run Launcher'
             ),
    
    Key([mod,"shift"] ,"r",
             lazy.spawn("rofi -combi-modi window,drun,ssh -theme solarized -show combi")
             ),
    Key([mod], "Tab",
             lazy.next_layout(),
             desc='Toggle through layouts'
             ),
    Key([mod, "shift"], "c",
             lazy.window.kill(),
             desc='Kill active window'
             ),
    Key([mod, "control"], "r",
             lazy.restart(),
             desc='Restart Qtile'
             ),
    Key([mod, "control"], "q",
             lazy.shutdown(),
             desc='Shutdown Qtile'
             ),
         ### Switch focus to specific monitor (out of three)
    Key([mod], "w",
             lazy.to_screen(0),
             desc='Keyboard focus to monitor 1'
             ),
    Key([mod], "e",
             lazy.to_screen(1),
             desc='Keyboard focus to monitor 2'
             ),
     Key([mod], "r",
             lazy.to_screen(2),
             desc='Keyboard focus to monitor 3'
             ),
         ### Switch focus of monitors
    Key([mod], "period",
             lazy.next_screen(),
             desc='Move focus to next monitor'
             ),
    Key([mod], "comma",
             lazy.prev_screen(),
             desc='Move focus to prev monitor'
             ),
         ### Treetab controls
    Key([mod, "control"], "k",
             lazy.layout.section_up(),
             desc='Move up a section in treetab'
             ),
    Key([mod, "control"], "j",
             lazy.layout.section_down(),
             desc='Move down a section in treetab'
             ),
         ### Window controls
    Key([mod], "k",
             lazy.layout.up()
             ),
    Key([mod], "j",
             lazy.layout.down()
             ),
    Key([mod, "shift"], "k",
             lazy.layout.shuffle_down(),
             desc='Move windows down in current stack'
             ),
    Key([mod, "shift"], "j",
             lazy.layout.shuffle_up(),
             desc='Move windows up in current stack'
             ),
    Key([mod], "h",
             lazy.layout.grow(),
             lazy.layout.increase_nmaster(),
             desc='Expand window (MonadTall), increase number in master pane (Tile)'
             ),
    Key([mod], "l",
             lazy.layout.shrink(),
             lazy.layout.decrease_nmaster(),
             desc='Shrink window (MonadTall), decrease number in master pane (Tile)'
             ),
    Key([mod], "n",
             lazy.layout.normalize(),
             desc='normalize window size ratios'
             ),
    Key([mod], "m",
             lazy.layout.maximize(),
             desc='toggle window between minimum and maximum sizes'
             ),
    Key([mod, "control"], "f",
             lazy.window.toggle_floating(),
             desc='toggle floating'
             ),
    Key([mod, "shift"], "m",
             lazy.window.toggle_fullscreen(),
             desc='toggle fullscreen'
             ),
         ### Stack controls
    Key([mod, "shift"], "space",
             lazy.layout.rotate(),
             lazy.layout.flip(),
             desc='Switch which side main pane occupies (XmonadTall)'
             ),
    Key([mod], "space",
             lazy.layout.next(),
             desc='Switch window focus to other pane(s) of stack'
             ),
    Key([mod, "control"], "Return",
             lazy.layout.toggle_split(),
             desc='Toggle between split and unsplit sides of stack'
             ),

    #### End DistroTube
    # Adjust Transparency 
    Key( ["mod1"], "k",lazy.spawn("compton-trans -c +10") ), 
    Key( ["mod1"], "j",lazy.spawn("compton-trans -c -10") ),

    # Adjust Volume
    Key( [] , "XF86AudioRaiseVolume",lazy.spawn("pactl set-sink-volume @DEFAULT_SINK@ +10%") ), 
    Key( [] , "XF86AudioLowerVolume",lazy.spawn("pactl set-sink-volume @DEFAULT_SINK@ -10%") ),
    Key( [] , "XF86AudioMute",lazy.spawn("pactl set-sink-mute @DEFAULT_SINK@ toggle") ),
        #  Key( [] , "XF86MicMute",lazy.spawn("pactl set-source-mute @DEFAULT_SOURCE@ toggle") ), 
   
    # Screenshot
    Key( [], "Print", lazy.spawn("flameshot gui")),
    Key( [mod], "Print", lazy.spawn("flameshot full -c")),
    
    # Start Browser
    Key([mod],"b",lazy.spawn("brave")),

    # Start Filemanager
    Key([mod],"f",lazy.spawn("pcmanfm")),

    # Lock Screen
    Key( ["mod1"], "l", lazy.spawn("i3lock-fancy-multimonitor") ),

    # Suspend and Lock
    Key( [mod,"mod1","shift"], "x", lazy.spawn("suspend-and-lock") ),
    # Log Out
    Key(["mod1", "control"], "x", lazy.spawn('arcolinux-logout')),

    #Emoji
    Key(["mod1"],"e",lazy.spawn("ibus emoji"))	
]


groups = [Group("1",label="", layout='tile'),
          Group("2",label="", matches=[Match(wm_class=["Org.gnome.Nautilus","Pcmanfm"])],layout='tile'),
          Group("3",label="", matches=[Match(wm_class=["firefox","qutebrowser","Nextcloud"])] ,layout='treetab'),
          Group("4",label="",layout='treetab'),
          Group("5",label="",layout='tile'),
          Group("6",label="",matches=[Match(wm_class=["MotionBox"])],layout='treetab'),
          Group("7",label="",layout='max'),
          Group("8",label="",layout='max',matches=[Match(wm_class=["Lutris"])]),
          Group("9",label="",layout='max',matches=[Match(wm_class=["discord"])]),
          Group("0",label="",layout='max',matches=[Match(wm_class=["Gimp-2.10"])]),
          Group("a"),
          Group("s"),
          Group("d"),
          Group("f"),
          Group("u"),
          Group("i"),
          Group("o"),
          Group("p")
         ]
for i in "1234567890asdfuiop":
    # go to a specific screen
    keys.append(
        Key([mod], i, lazy.group[i].toscreen())
    )
    # take the current window to a specific screen
    keys.append(
        Key([mod,"shift"], i, lazy.window.togroup(i))
    )

##### COLORS #####
file = list()
with open('/home/decisivedove/.cache/wal/colors') as lines:
        file = lines.read().splitlines()
colors = [[file[0], file[0]], # panel background
          [file[15], file[15]], # background for current screen tab
          [file[4], file[4]], # font color for group names
          [file[1], file[1]], # border line color for current tab
          [file[1], file[1]], # border line color for other tab and odd widgets
          [file[2], file[2]], # color for the even widgets
          [file[1], file[1]]] # window name

layout_theme = {"border_width": 10,
                "margin": 20,
                "border_focus": colors[5][0],
                "border_normal": colors[3][0]
                }

layouts = [
    layout.MonadWide(**layout_theme),
    layout.Bsp(**layout_theme),
    layout.Stack(stacks=2, **layout_theme),
    layout.Columns(**layout_theme),
    layout.RatioTile(**layout_theme),
    layout.VerticalTile(**layout_theme),
    layout.Matrix(**layout_theme),
    layout.Zoomy(**layout_theme),
    layout.MonadTall(**layout_theme),
    layout.Max(**layout_theme),
    layout.Tile(shift_windows=True, **layout_theme),
    layout.Stack(num_stacks=2),
    layout.TreeTab(
         font = "Ubuntu",
         fontsize = 10,
         sections = ["FIRST", "SECOND"],
         section_fontsize = 11,
         bg_color = "141414",
         active_bg = "90C435",
         active_fg = "000000",
         inactive_bg = "384323",
         inactive_fg = "a0a0a0",
         padding_y = 5,
         section_top = 10,
         panel_width = 320
         ),
    layout.Floating(**layout_theme)
]



widget_defaults = dict(
    font="Ubuntu Mono",
    fontsize=12,
    padding=3,
)
extension_defaults = widget_defaults.copy()



# Drag floating layouts.
mouse = [
    Drag([mod], "Button1", lazy.window.set_position_floating(),
         start=lazy.window.get_position()),
    Drag([mod], "Button3", lazy.window.set_size_floating(),
         start=lazy.window.get_size()),
    Click([mod], "Button2", lazy.window.bring_to_front())
]



screens = [
    Screen(
        top=bar.Bar(
            [
                widget.Sep(
                         linewidth = 0,
                         padding = 17,
                         foreground = colors[2],
                         background = colors[0]
                        ),


                widget.GroupBox(font="Font Awesome",
                        fontsize = 16,
                        margin_y = 3,
                        margin_x = 0,
                        padding_y = 5,
                        padding_x = 5,
                        borderwidth = 3,
                        active = colors[2],
                        inactive = colors[1],
                        rounded = False,
                        highlight_color = file[0],
                        highlight_method = "line",
                        this_current_screen_border = colors[3],
                        this_screen_border = colors [4],
                        other_current_screen_border = colors[0],
                        other_screen_border = colors[0],
                        foreground = colors[2],
                        background = colors[0]
                        ),


                widget.Sep(
                        linewidth = 0,
                        padding = 6,
                        foreground = colors[2],
                        background = colors[0]
                 ),
                widget.TextBox(
                        text='',
                        background = colors[5],
                        foreground = colors[0],
                        padding=0,
                        fontsize=47
                        ),

                widget.WindowName(
                        foreground = colors[1],
                        background = colors[5],
                        ),
                
                widget.TextBox(
                        text='',
                        background = colors[0],
                        foreground = colors[5],
                        padding=0,
                        fontsize=47
                        ),
                widget.Sep(
                        linewidth = 0,
                        padding = 250,
                        foreground = colors[0],
                        background = colors[0]
                 ),

                widget.TextBox(
                        text='',
                        background = colors[0],
                        foreground = colors[4],
                        padding=0,
                        fontsize=37
                        ),
                widget.CPU(
                        foreground=colors[1],
                        background=colors[4],
                        padding = 5,
			mouse_callbacks = {'Button1': lambda qtile: qtile.cmd_spawn( myTerm + ' -e htop')}
                         ),

		widget.TextBox(
                        text='',
                        background = colors[4],
                        foreground = colors[5],
                        padding=0,
                        fontsize=37
                        ),


                widget.Net(
                       interface = "enp8s0",
                       format = '{down} ↓↑ {up}',
                       foreground = colors[1],
                       background = colors[5],
                       padding = 5,
		       mouse_callbacks = {'Button1': lambda qtile: qtile.cmd_spawn('nm-connection-editor')}
                        ),
		widget.TextBox(
                            text='',
                            background = colors[5],
                            foreground = colors[4],
                            padding=0,
                            fontsize=37
                            ),
		widget.Memory(
                        foreground = colors[1],
                        background = colors[4],
                        padding = 5,
			format = '{MemUsed}M',
			mouse_callbacks = {'Button1': lambda qtile: qtile.cmd_spawn(myTerm +' -e htop')}
                        ),
		
                widget.TextBox(
                        text='',
                        background = colors[4],
                        foreground = colors[5],
                        padding=0,
                        fontsize=37
                        ),
	       widget.Clock(
                        foreground = colors[1],
                        background = colors[5],
                        format="%A, %B %d  [ %I:%M  %p]"
                        ),

                widget.TextBox(
                        text='',
                        background = colors[5],
                        foreground = colors[4],
                        padding=0,
                        fontsize=37
                        ),

                widget.TextBox(
                        text='Vol:',
                        background = colors[4],
                        foreground = colors[1],
                        padding=0,
                        fontsize=12
                        ),

		widget.Volume(
                        foreground = colors[1],
                        background = colors[4],
                        padding = 5
                        ),

		widget.TextBox(
                        text='',
                        background = colors[4],
                        foreground = colors[5],
                        padding=0,
                        fontsize=37
                        ),

 
                widget.CurrentLayout(
                        foreground = colors[1],
                        background = colors[5],
                        padding = 5
                        ),

		widget.TextBox(
                        text='',
                        background = colors[5],
                        foreground = colors[4],
                        padding=0,
                        fontsize=37
                        ),


		widget.Systray(
                        background=colors[4],
                        padding = 5
                        ),
                widget.Sep(
                         linewidth = 0,
                         padding = 17,
                         foreground = colors[2],
                         background = colors[4]
                        ),

		 ],
            24,
        ),
    ),
]


dgroups_key_binder = None
dgroups_app_rules = []  # type: List
main = None
follow_mouse_focus = True
bring_front_click = False
cursor_warp = False
floating_layout = layout.Floating(float_rules=[
    # Run the utility of `xprop` to see the wm class and name of an X client.
    {'wmclass': 'confirm'},
    {'wmclass': 'dialog'},
    {'wmclass': 'download'},
    {'wmclass': 'error'},
    {'wmclass': 'file_progress'},
    {'wmclass': 'notification'},
    {'wmclass': 'splash'},
    {'wmclass': 'toolbar'},
    {'wmclass': 'confirmreset'},  # gitk
    {'wmclass': 'makebranch'},  # gitk
    {'wmclass': 'maketag'},  # gitk
    {'wname': 'branchdialog'},  # gitk
    {'wname': 'pinentry'},  # GPG key password entry
    {'wmclass': 'ssh-askpass'},  # ssh-askpass
])
auto_fullscreen = True
focus_on_window_activation = "smart"

# XXX: Gasp! We're lying here. In fact, nobody really uses or cares about this
# string besides java UI toolkits; you can see several discussions on the
# mailing lists, GitHub issues, and other WM documentation that suggest setting
# this string if your java app doesn't work correctly. We may as well just lie
# and say that we're a working one by default.
#
# We choose LG3D to maximize irony: it is a 3D non-reparenting WM written in
# java that happens to be on java's whitelist.
# wmname = "LG3D" 
# Uncomment the above line and delete the below if your Java app does not work
wmname = "Qtile"

