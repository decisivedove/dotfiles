const char *colorname[] = {

  /* 8 normal colors */
  [0] = "#0A0918", /* black   */
  [1] = "#284D46", /* red     */
  [2] = "#B9434D", /* green   */
  [3] = "#26B173", /* yellow  */
  [4] = "#F0B758", /* blue    */
  [5] = "#155E9E", /* magenta */
  [6] = "#1A76CC", /* cyan    */
  [7] = "#8fd6d5", /* white   */

  /* 8 bright colors */
  [8]  = "#649595",  /* black   */
  [9]  = "#284D46",  /* red     */
  [10] = "#B9434D", /* green   */
  [11] = "#26B173", /* yellow  */
  [12] = "#F0B758", /* blue    */
  [13] = "#155E9E", /* magenta */
  [14] = "#1A76CC", /* cyan    */
  [15] = "#8fd6d5", /* white   */

  /* special colors */
  [256] = "#0A0918", /* background */
  [257] = "#8fd6d5", /* foreground */
  [258] = "#8fd6d5",     /* cursor */
};

/* Default colors (colorname index)
 * foreground, background, cursor */
 unsigned int defaultbg = 0;
 unsigned int defaultfg = 257;
 unsigned int defaultcs = 258;
 unsigned int defaultrcs= 258;
