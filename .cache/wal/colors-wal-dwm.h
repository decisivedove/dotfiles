static const char norm_fg[] = "#8fd6d5";
static const char norm_bg[] = "#0A0918";
static const char norm_border[] = "#649595";

static const char sel_fg[] = "#8fd6d5";
static const char sel_bg[] = "#B9434D";
static const char sel_border[] = "#8fd6d5";

static const char urg_fg[] = "#8fd6d5";
static const char urg_bg[] = "#284D46";
static const char urg_border[] = "#284D46";

static const char *colors[][3]      = {
    /*               fg           bg         border                         */
    [SchemeNorm] = { norm_fg,     norm_bg,   norm_border }, // unfocused wins
    [SchemeSel]  = { sel_fg,      sel_bg,    sel_border },  // the focused win
    [SchemeUrg] =  { urg_fg,      urg_bg,    urg_border },
};
