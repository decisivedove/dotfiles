static const char* selbgcolor   = "#0A0918";
static const char* selfgcolor   = "#8fd6d5";
static const char* normbgcolor  = "#B9434D";
static const char* normfgcolor  = "#8fd6d5";
static const char* urgbgcolor   = "#284D46";
static const char* urgfgcolor   = "#8fd6d5";
