static const char *colors[SchemeLast][2] = {
	/*     fg         bg       */
	[SchemeNorm] = { "#8fd6d5", "#0A0918" },
	[SchemeSel] = { "#8fd6d5", "#284D46" },
	[SchemeOut] = { "#8fd6d5", "#1A76CC" },
};
